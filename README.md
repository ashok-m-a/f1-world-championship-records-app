# F1 World Championship Records App

This app displays f1 world championship records using Ergast Developer API.

## Getting Started

Once the dependencies installed, you can launch the app in your local by running the below command.

```
npm run start
```

You can refer the live app deployed here [F1 World Championship Records App](https://f1-world-records-app.cfapps.io).

### Prerequisites

I have used `npm` package manager. Please visit the site [node & npm](https://nodejs.org).

Once the package manager is ready, install the dependencies by running the below command.

```
npm install
```

### Installing

To run a development server enabled with hot deployment in your local, please follow the below steps.

* Start the development server.

    ```
    npm run start:server
    ```

* Watching for changes with hot deployment.

    ```
    npm run start:watch
    ```

## Running the tests

I favor e2e over unit tests. So please follow the below to run protractor e2e tests.

### Start the webdriver

Selenium webdriver is needed to run our tests in browsers. To start a webdriver run the below command.

    ```
    npm run start:webdriver
    ```

### Run Protractor E2E Tests

Now we can run our e2e tests using the following command:

    ```
    npm run e2e
    ```

## Built With

* [AngularJS](https://angularjs.org/) - The web framework used.
* [Typescript](https://www.typescriptlang.org/) - Static Typing.
* [Bootstrap](https://getbootstrap.com/docs/3.3/) - CSS Styles.
* [Webpack 4](https://webpack.js.org/) - Build & Run.
* [Protractor](https://www.protractortest.org/) - E2E Testing.

## Contributing

Not there yet. ;-) I will let you know.

## Versioning

I used [Bitbucket](https://bitbucket.org) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/ashok-m-a/f1-world-championship-records-app).

## Author

* **Ashok M A** - [Bitbucket profile](https://bitbucket.org/ashok-m-a/)

## License

I am not sure under which license this will come. >_<

## Acknowledgments

* Thanks to [Ergast API](http://ergast.com)
* Thanks to [PurpleBooth](https://github.com/PurpleBooth) for giving an information about README template.
