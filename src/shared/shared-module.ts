import {module} from 'angular';
import {NavController} from './nav-controller';

export const sharedModule = module('formulaOneRecordsApp.sharedModule', []);

/**
 * I added this controller to make the route work during a static HTML access. :'(
 * specifying the path in <a href> is not working due to relative path access with file:// protocol.
 */
sharedModule.controller(NavController.controllerId, NavController);
