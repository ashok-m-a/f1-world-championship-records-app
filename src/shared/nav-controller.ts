import {ILocationService} from 'angular';

export class NavController {
    public static $inject = ['$location'];
    public static controllerId = 'navController';

    constructor(private $location: ILocationService) {
    }

    private switchView(viewName: string) {
        this.$location.path(viewName);
    }
}
