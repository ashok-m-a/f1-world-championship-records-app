import {Champion} from './Champion';
import {SeasonList} from './SeasonList';

const DEFAULT_ROWS = 15;

export class ChampionsListModel {
    public activePage: number;
    public seasonList: SeasonList;
    public champions: Champion[];

    public getPagination = () => {
        const pages = [];
        for (let page = 1; page <= Math.ceil(this.champions.length / DEFAULT_ROWS); page++) {
            pages.push(page);
        }
        return pages;
    };

    private getChampions() {
        const startIndex = ((this.activePage - 1) * DEFAULT_ROWS);
        const pageLimit = (this.activePage * DEFAULT_ROWS);
        return this.champions.filter((value, index) => startIndex <= index && index < pageLimit);
    }
}
