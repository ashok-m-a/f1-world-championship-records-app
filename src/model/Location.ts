export class Location {
    public locality: string;
    public country: string;

    public getFullLocation = () => `${this.locality}, ${this.country}`;
}
