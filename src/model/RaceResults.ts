import {CircuitDetails} from './CircuitDetails';
import {DriverDetails} from './DriverDetails';

export class RaceResults {
    public raceName: string;
    public winner: DriverDetails;
    public circuit: CircuitDetails;
}
