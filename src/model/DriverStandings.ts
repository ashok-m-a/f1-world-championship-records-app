import {DriverDetails} from './DriverDetails';
import {Sponsor} from './Sponsor';

export class DriverStandings {
    public driver: DriverDetails;
    public sponsor: Sponsor; // Constructor is a key word ;')
    public points: number;
    public wins: number;
}
