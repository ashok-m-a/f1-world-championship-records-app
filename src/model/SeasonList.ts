export class SeasonList {
    public seasons: number[];

    public getSeasonsFrom = (periodStart: string) =>
        this.seasons.filter((season) => season > Number(periodStart));

}
