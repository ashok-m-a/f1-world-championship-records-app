import {Champion} from './Champion';
import {RaceResults} from './RaceResults';

export class SeasonResultsModel {
    public champion: Champion;
    public races: RaceResults[];
}
