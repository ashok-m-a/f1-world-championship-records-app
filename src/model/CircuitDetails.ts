import {Location} from './Location';

export class CircuitDetails {
    public circuitName: string;
    public location: Location;
}
