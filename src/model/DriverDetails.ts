export class DriverDetails {
  public driverId: string;
  public givenName: string;
  public familyName: string;
}
