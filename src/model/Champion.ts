import {DriverStandings} from './DriverStandings';

export class Champion {
    public season: number;
    public driverStandings: DriverStandings;
}
