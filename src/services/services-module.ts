import * as angular from 'angular';
import {ChampionService} from './champion-service';
import {RaceService} from './race-service';
import {SeasonService} from './season-service';

export const servicesModule = angular.module('formulaOneRecordsApp.servicesModule', []);
servicesModule.service(ChampionService.serviceId, ChampionService);
servicesModule.service(RaceService.serviceId, RaceService);
servicesModule.service(SeasonService.serviceId, SeasonService);
