import {
  extend,
  IHttpService,
  IQService,
  ISCEService,
  IWindowService
} from 'angular';
import { Champion } from '../model/Champion';
import { DriverDetails } from '../model/DriverDetails';
import { DriverStandings } from '../model/DriverStandings';
import { Sponsor } from '../model/Sponsor';

export class ChampionService implements IChampionService {
  public static $inject = ['$window', '$q', '$http', '$sce'];
  public static serviceId = 'championService';

  constructor(
    private $window: IWindowService,
    private $q: IQService,
    private $http: IHttpService,
    private $sce: ISCEService
  ) {}

  public retrieveChampion(year: number): Champion {
    let champion = new Champion();

    if (this.$window.localStorage.getItem(year)) {
      return (champion = JSON.parse(this.$window.localStorage.getItem(year)));
    }

    champion.season = year;

    /**
     * {@link ISCEService} is needed here to mark and wrap the thirty party api as trusted one, since angular 1.6.x.
     */
    const driverStandingsResp = this.$http
      .jsonp(
        this.$sce.trustAsResourceUrl(
          `https://ergast.com/api/f1/${year}/driverStandings.json`
        ),
        { jsonpCallbackParam: 'callback', cache: true }
      )
      .then((resp: any) => resp.data);

    this.$q.all([driverStandingsResp]).then((results: any) => {
      const standingsList = results[0].MRData.StandingsTable.StandingsLists[0];
      standingsList &&
        populateChampion(standingsList.DriverStandings[0], champion); // short-circuit expression.
      this.$window.localStorage.setItem(year, JSON.stringify(champion));
    });
    return champion;
  }
}

export interface IChampionService {
  retrieveChampion(year: number): Champion;
}

function populateChampion(driverStandingData: any, champion: Champion) {
  const driverStandings = new DriverStandings();
  const driver = new DriverDetails();
  const sponsor = new Sponsor();
  extend(sponsor, driverStandingData.Constructors[0]);
  extend(driver, driverStandingData.Driver);
  driverStandings.points = driverStandingData.points;
  driverStandings.wins = driverStandingData.wins;
  driverStandings.driver = driver;
  driverStandings.sponsor = sponsor;
  champion.driverStandings = driverStandings;
}
