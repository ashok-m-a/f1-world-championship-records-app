import {
  extend,
  IHttpService,
  IQService,
  ISCEService,
  IWindowService
} from 'angular';
import { CircuitDetails } from '../model/CircuitDetails';
import { DriverDetails } from '../model/DriverDetails';
import { Location } from '../model/Location';
import { RaceResults } from '../model/RaceResults';

export class RaceService implements IRaceService {
  public static $inject = ['$window', '$http', '$q', '$sce'];
  public static serviceId = 'raceService';

  constructor(
    private $window: IWindowService,
    private $http: IHttpService,
    private $q: IQService,
    private $sce: ISCEService
  ) {}

  public retrieveRaceResults(season: string): RaceResults[] {
    const races: RaceResults[] = [];

    // if (this.$window.localStorage.getItem(season)) {
    //   console.log(this.$window.localStorage.getItem(season));
    //   return (races = JSON.parse(this.$window.localStorage.getItem(season))
    //     .races);
    // }

    /**
     * {@link ISCEService} is needed here to mark and wrap the thirty party api as trusted one, since angular 1.6.x.
     */
    const seasonResultsResp = this.$http
      .jsonp(
        this.$sce.trustAsResourceUrl(
          `https://ergast.com/api/f1/${season}/results/1.json`
        ),
        { jsonpCallbackParam: 'callback', cache: true }
      )
      .then((resp: any) => resp.data);

    this.$q.all([seasonResultsResp]).then((results: any) => {
      const raceResultsData: any[] = results[0].MRData.RaceTable.Races;
      raceResultsData.forEach(race => races.push(populateRaceResults(race)));
    //   this.$window.localStorage.setItem(season, `{${JSON.stringify(races)}}`);
    });

    return races;
  }
}

export interface IRaceService {
  retrieveRaceResults(season: string): RaceResults[];
}

function populateRaceResults(race: any): RaceResults {
  const raceResults = new RaceResults();
  raceResults.raceName = race.raceName;
  const circuit = new CircuitDetails();
  circuit.circuitName = race.Circuit.circuitName;
  const location = new Location();
  extend(location, race.Circuit.Location);
  circuit.location = location;
  raceResults.circuit = circuit;
  const driver = new DriverDetails();
  extend(driver, race.Results[0].Driver);
  raceResults.winner = driver;
  return raceResults;
}
