import { IHttpService, IQService, ISCEService, IWindowService } from 'angular';
import { SeasonList } from '../model/SeasonList';

const MAX_LIMIT = 100; // This can be dynamically set by retrieving the totalSeasonsAvailable first.
const SEASONS_STORAGE_KEY = 'seasons';

export class SeasonService implements ISeasonService {
  public static $inject = ['$window', '$http', '$q', '$sce'];
  public static serviceId = 'seasonService';

  private seasonListUrl: string;

  constructor(
    private $window: IWindowService,
    private $http: IHttpService,
    private $q: IQService,
    private $sce: ISCEService
  ) {
    /**
     * {@link ISCEService} is needed here to mark and wrap the thirty party api as trusted one, since angular 1.6.x.
     */
    this.seasonListUrl = this.$sce.trustAsResourceUrl(
      'https://ergast.com/api/f1/seasons.json'
    );
  }

  public retrieveSeasons(): SeasonList {
    const seasonList = new SeasonList();

    if (this.$window.localStorage.getItem(SEASONS_STORAGE_KEY)) {
      seasonList.seasons = JSON.parse(
        this.$window.localStorage.getItem(SEASONS_STORAGE_KEY)
      );
      return seasonList;
    }

    seasonList.seasons = [];

    const seasonListResp = this.$http
      .jsonp(this.seasonListUrl, {
        jsonpCallbackParam: 'callback',
        params: { limit: MAX_LIMIT },
        cache: true
      })
      .then((resp: any) => resp.data);

    this.$q.all([seasonListResp]).then((results: any) => {
      const seasons: any[] = results[0].MRData.SeasonTable.Seasons;
      seasons.forEach(seasonObj => seasonList.seasons.push(seasonObj.season));
      this.$window.localStorage.setItem(
        SEASONS_STORAGE_KEY,
        JSON.stringify(seasonList.seasons)
      );
    });

    return seasonList;
  }
}

export interface ISeasonService {
  retrieveSeasons(): SeasonList;
}
