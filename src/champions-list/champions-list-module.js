Object.defineProperty(exports, "__esModule", { value: true });
var angular = require("angular");
var champions_list_controller_1 = require("./champions-list-controller");
exports.championsListModule = angular.module('formulaOneRecordsApp.championsListModule', []);
exports.championsListModule.controller(champions_list_controller_1.ChampionsListController.controllerId, champions_list_controller_1.ChampionsListController);
