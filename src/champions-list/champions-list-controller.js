Object.defineProperty(exports, "__esModule", { value: true });
var ChampionsListModel_1 = require("../model/ChampionsListModel");
var champion_service_1 = require("../services/champion-service");
var season_service_1 = require("../services/season-service");
var ChampionsListController = /** @class */ (function () {
    function ChampionsListController(seasonService, championService) {
        this.seasonService = seasonService;
        this.championService = championService;
        this.pageTitle = 'Champions List';
        this.periodStart = '2005';
        this.periodEnd = '2015';
        this.model = new ChampionsListModel_1.ChampionsListModel();
        this.init();
        this.model.activePage = 1;
    }
    ChampionsListController.prototype.init = function () {
        this.model.seasonList = this.seasonService.retrieveSeasons();
        this.populateChampions();
    };
    ChampionsListController.prototype.populateChampions = function () {
        this.model.champions = [];
        for (var year = Number(this.periodStart); year <= Number(this.periodEnd); year++) {
            this.model.champions.push(this.championService.retrieveChampion(year));
        }
    };
    ChampionsListController.$inject = [season_service_1.SeasonService.serviceId, champion_service_1.ChampionService.serviceId];
    ChampionsListController.controllerId = 'championsListController';
    return ChampionsListController;
}());
exports.ChampionsListController = ChampionsListController;
