import * as angular from 'angular';

import {ChampionsListController} from './champions-list-controller';

export const championsListModule = angular.module('formulaOneRecordsApp.championsListModule', []);
championsListModule.controller(ChampionsListController.controllerId, ChampionsListController);
