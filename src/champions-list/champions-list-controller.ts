import {ChampionsListModel} from '../model/ChampionsListModel';
import {ChampionService, IChampionService} from '../services/champion-service';
import {ISeasonService, SeasonService} from '../services/season-service';

export class ChampionsListController {
    public static $inject = [SeasonService.serviceId, ChampionService.serviceId];
    public static controllerId = 'championsListController';

    private pageTitle = 'Champions List';
    private periodStart = '2005';
    private periodEnd = '2015';
    private model = new ChampionsListModel();

    constructor(private seasonService: ISeasonService, private championService: IChampionService) {
        this.init();
        this.model.activePage = 1;
    }

    private init() {
        this.model.seasonList = this.seasonService.retrieveSeasons();

        this.populateChampions();
    }

    private populateChampions() {
        this.model.champions = [];
        for (let year = Number(this.periodStart); year <= Number(this.periodEnd); year++) {
            this.model.champions.push(this.championService.retrieveChampion(year));
        }
    }

}
