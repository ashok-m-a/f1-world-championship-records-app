import {module} from 'angular';
import '../assets/images/logo.png';
import '../assets/sass/app.scss';
import {championsListModule} from './champions-list/champions-list-module';
import {appRoutes} from './configs/app-routes';

import {seasonResultsModule} from './season-results/season-results-module';
import {servicesModule} from './services/services-module';
import {sharedModule} from './shared/shared-module';

const formulaOneRecordsApp = module('formulaOneRecordsApp',
    [
        'ngRoute',
        championsListModule.name,
        seasonResultsModule.name,
        sharedModule.name,
        servicesModule.name]);

formulaOneRecordsApp.config(appRoutes);
