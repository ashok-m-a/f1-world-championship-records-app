import {ChampionsListController} from '../champions-list/champions-list-controller';
import {SeasonResultsController} from '../season-results/season-results-controller';

const welcomeTemplate: string = require('../welcome/welcome.html');
const championsListTemplate: string = require('../champions-list/champions-list.html');
const seasonResultsTemplate: string = require('../season-results/season-results.html');

appRoutes.$inject = ['$routeProvider'];

export function appRoutes($routeProvider: ng.route.IRouteProvider) {
    $routeProvider
        .when('/welcome', {template: welcomeTemplate})
        .when('/champions', {template: championsListTemplate, controller: ChampionsListController.controllerId, controllerAs: 'vm'})
        .when('/seasonResults/:season', {template: seasonResultsTemplate, controller: SeasonResultsController.controllerId, controllerAs: 'vm'})
        .otherwise({redirectTo: '/welcome'});
}
