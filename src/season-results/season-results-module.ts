import * as angular from 'angular';
import {SeasonResultsController} from './season-results-controller';

export const seasonResultsModule = angular.module('formulaOneRecordsApp.seasonResultsModule', []);
seasonResultsModule.controller(SeasonResultsController.controllerId, SeasonResultsController);
