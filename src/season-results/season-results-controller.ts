import {SeasonResultsModel} from '../model/SeasonResultsModel';
import {ChampionService, IChampionService} from '../services/champion-service';
import {IRaceService, RaceService} from '../services/race-service';

export class SeasonResultsController {
    public static $inject = ['$routeParams', RaceService.serviceId, ChampionService.serviceId];
    public static controllerId = 'seasonResultsController';

    private pageTitle: string;
    private season: string;
    private model = new SeasonResultsModel();

    constructor(private $routeParams: ng.route.IRouteParamsService,
                private raceService: IRaceService,
                private championService: IChampionService)
    {
        this.season = $routeParams.season;
        this.pageTitle = `Season Results: ${this.season}`;

        this.init();
    }

    private init() {
        this.model.races = this.raceService.retrieveRaceResults(this.season);
        this.model.champion = this.championService.retrieveChampion(Number(this.season));
    }

    private isChampion = (driverId: string): boolean =>
        this.model.champion.driverStandings && this.model.champion.driverStandings.driver.driverId === driverId;

}
