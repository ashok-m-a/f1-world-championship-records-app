const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    resolve: {
        extensions: ['.html', '.ts', '.js']
    }, entry: './src/index.ts',
    output: {
        path: __dirname + '/dist/',
        filename: 'js/bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                loader: 'ng-cache-loader?-prefix'
            },
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader'
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: { name: '[path][name].[ext]' }
                    }
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: 'style-loader' // creates style nodes from JS strings
                }, {
                    loader: 'css-loader' // translates CSS into CommonJS
                }, {
                    loader: 'sass-loader' // compiles Sass to CSS
                }]
            }
        ]
    }, plugins: [
        new CopyWebpackPlugin([
            {
                from: 'assets/images'
            }
        ])
    ],
    devServer: {
        contentBase: './'
    },
    externals: {
        angular: 'angular'
    }
};
