/******/ (function(modules) { // webpackBootstrap
/******/ 	function hotDisposeChunk(chunkId) {
/******/ 		delete installedChunks[chunkId];
/******/ 	}
/******/ 	var parentHotUpdateCallback = window["webpackHotUpdate"];
/******/ 	window["webpackHotUpdate"] = // eslint-disable-next-line no-unused-vars
/******/ 	function webpackHotUpdateCallback(chunkId, moreModules) {
/******/ 		hotAddUpdateChunk(chunkId, moreModules);
/******/ 		if (parentHotUpdateCallback) parentHotUpdateCallback(chunkId, moreModules);
/******/ 	} ;
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotDownloadUpdateChunk(chunkId) {
/******/ 		var head = document.getElementsByTagName("head")[0];
/******/ 		var script = document.createElement("script");
/******/ 		script.charset = "utf-8";
/******/ 		script.src = __webpack_require__.p + "" + chunkId + "." + hotCurrentHash + ".hot-update.js";
/******/ 		;
/******/ 		head.appendChild(script);
/******/ 	}
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotDownloadManifest(requestTimeout) {
/******/ 		requestTimeout = requestTimeout || 10000;
/******/ 		return new Promise(function(resolve, reject) {
/******/ 			if (typeof XMLHttpRequest === "undefined")
/******/ 				return reject(new Error("No browser support"));
/******/ 			try {
/******/ 				var request = new XMLHttpRequest();
/******/ 				var requestPath = __webpack_require__.p + "" + hotCurrentHash + ".hot-update.json";
/******/ 				request.open("GET", requestPath, true);
/******/ 				request.timeout = requestTimeout;
/******/ 				request.send(null);
/******/ 			} catch (err) {
/******/ 				return reject(err);
/******/ 			}
/******/ 			request.onreadystatechange = function() {
/******/ 				if (request.readyState !== 4) return;
/******/ 				if (request.status === 0) {
/******/ 					// timeout
/******/ 					reject(
/******/ 						new Error("Manifest request to " + requestPath + " timed out.")
/******/ 					);
/******/ 				} else if (request.status === 404) {
/******/ 					// no update available
/******/ 					resolve();
/******/ 				} else if (request.status !== 200 && request.status !== 304) {
/******/ 					// other failure
/******/ 					reject(new Error("Manifest request to " + requestPath + " failed."));
/******/ 				} else {
/******/ 					// success
/******/ 					try {
/******/ 						var update = JSON.parse(request.responseText);
/******/ 					} catch (e) {
/******/ 						reject(e);
/******/ 						return;
/******/ 					}
/******/ 					resolve(update);
/******/ 				}
/******/ 			};
/******/ 		});
/******/ 	}
/******/
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "86a98a8ba3d2feb5f07e"; // eslint-disable-line no-unused-vars
/******/ 	var hotRequestTimeout = 10000;
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentChildModule; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParentsTemp = []; // eslint-disable-line no-unused-vars
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotCreateRequire(moduleId) {
/******/ 		var me = installedModules[moduleId];
/******/ 		if (!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if (me.hot.active) {
/******/ 				if (installedModules[request]) {
/******/ 					if (!installedModules[request].parents.includes(moduleId))
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 				} else {
/******/ 					hotCurrentParents = [moduleId];
/******/ 					hotCurrentChildModule = request;
/******/ 				}
/******/ 				if (!me.children.includes(request)) me.children.push(request);
/******/ 			} else {
/******/ 				console.warn(
/******/ 					"[HMR] unexpected require(" +
/******/ 						request +
/******/ 						") from disposed module " +
/******/ 						moduleId
/******/ 				);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		var ObjectFactory = function ObjectFactory(name) {
/******/ 			return {
/******/ 				configurable: true,
/******/ 				enumerable: true,
/******/ 				get: function() {
/******/ 					return __webpack_require__[name];
/******/ 				},
/******/ 				set: function(value) {
/******/ 					__webpack_require__[name] = value;
/******/ 				}
/******/ 			};
/******/ 		};
/******/ 		for (var name in __webpack_require__) {
/******/ 			if (
/******/ 				Object.prototype.hasOwnProperty.call(__webpack_require__, name) &&
/******/ 				name !== "e"
/******/ 			) {
/******/ 				Object.defineProperty(fn, name, ObjectFactory(name));
/******/ 			}
/******/ 		}
/******/ 		fn.e = function(chunkId) {
/******/ 			if (hotStatus === "ready") hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			return __webpack_require__.e(chunkId).then(finishChunkLoading, function(err) {
/******/ 				finishChunkLoading();
/******/ 				throw err;
/******/ 			});
/******/
/******/ 			function finishChunkLoading() {
/******/ 				hotChunksLoading--;
/******/ 				if (hotStatus === "prepare") {
/******/ 					if (!hotWaitingFilesMap[chunkId]) {
/******/ 						hotEnsureUpdateChunk(chunkId);
/******/ 					}
/******/ 					if (hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 						hotUpdateDownloaded();
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 		return fn;
/******/ 	}
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotCreateModule(moduleId) {
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 			_main: hotCurrentChildModule !== moduleId,
/******/
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if (typeof dep === "undefined") hot._selfAccepted = true;
/******/ 				else if (typeof dep === "function") hot._selfAccepted = dep;
/******/ 				else if (typeof dep === "object")
/******/ 					for (var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback || function() {};
/******/ 				else hot._acceptedDependencies[dep] = callback || function() {};
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if (typeof dep === "undefined") hot._selfDeclined = true;
/******/ 				else if (typeof dep === "object")
/******/ 					for (var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 				else hot._declinedDependencies[dep] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if (idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if (!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if (idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		hotCurrentChildModule = undefined;
/******/ 		return hot;
/******/ 	}
/******/
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for (var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailableFilesMap = {};
/******/ 	var hotDeferred;
/******/
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = +id + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/
/******/ 	function hotCheck(apply) {
/******/ 		if (hotStatus !== "idle")
/******/ 			throw new Error("check() is only allowed in idle status");
/******/ 		hotApplyOnUpdate = apply;
/******/ 		hotSetStatus("check");
/******/ 		return hotDownloadManifest(hotRequestTimeout).then(function(update) {
/******/ 			if (!update) {
/******/ 				hotSetStatus("idle");
/******/ 				return null;
/******/ 			}
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			hotAvailableFilesMap = update.c;
/******/ 			hotUpdateNewHash = update.h;
/******/
/******/ 			hotSetStatus("prepare");
/******/ 			var promise = new Promise(function(resolve, reject) {
/******/ 				hotDeferred = {
/******/ 					resolve: resolve,
/******/ 					reject: reject
/******/ 				};
/******/ 			});
/******/ 			hotUpdate = {};
/******/ 			var chunkId = "main";
/******/ 			{
/******/ 				// eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if (
/******/ 				hotStatus === "prepare" &&
/******/ 				hotChunksLoading === 0 &&
/******/ 				hotWaitingFiles === 0
/******/ 			) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 			return promise;
/******/ 		});
/******/ 	}
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) {
/******/ 		if (!hotAvailableFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for (var moduleId in moreModules) {
/******/ 			if (Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if (--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if (!hotAvailableFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var deferred = hotDeferred;
/******/ 		hotDeferred = null;
/******/ 		if (!deferred) return;
/******/ 		if (hotApplyOnUpdate) {
/******/ 			// Wrap deferred object in Promise to mark it as a well-handled Promise to
/******/ 			// avoid triggering uncaught exception warning in Chrome.
/******/ 			// See https://bugs.chromium.org/p/chromium/issues/detail?id=465666
/******/ 			Promise.resolve()
/******/ 				.then(function() {
/******/ 					return hotApply(hotApplyOnUpdate);
/******/ 				})
/******/ 				.then(
/******/ 					function(result) {
/******/ 						deferred.resolve(result);
/******/ 					},
/******/ 					function(err) {
/******/ 						deferred.reject(err);
/******/ 					}
/******/ 				);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for (var id in hotUpdate) {
/******/ 				if (Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			deferred.resolve(outdatedModules);
/******/ 		}
/******/ 	}
/******/
/******/ 	function hotApply(options) {
/******/ 		if (hotStatus !== "ready")
/******/ 			throw new Error("apply() is only allowed in ready status");
/******/ 		options = options || {};
/******/
/******/ 		var cb;
/******/ 		var i;
/******/ 		var j;
/******/ 		var module;
/******/ 		var moduleId;
/******/
/******/ 		function getAffectedStuff(updateModuleId) {
/******/ 			var outdatedModules = [updateModuleId];
/******/ 			var outdatedDependencies = {};
/******/
/******/ 			var queue = outdatedModules.slice().map(function(id) {
/******/ 				return {
/******/ 					chain: [id],
/******/ 					id: id
/******/ 				};
/******/ 			});
/******/ 			while (queue.length > 0) {
/******/ 				var queueItem = queue.pop();
/******/ 				var moduleId = queueItem.id;
/******/ 				var chain = queueItem.chain;
/******/ 				module = installedModules[moduleId];
/******/ 				if (!module || module.hot._selfAccepted) continue;
/******/ 				if (module.hot._selfDeclined) {
/******/ 					return {
/******/ 						type: "self-declined",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				if (module.hot._main) {
/******/ 					return {
/******/ 						type: "unaccepted",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				for (var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if (!parent) continue;
/******/ 					if (parent.hot._declinedDependencies[moduleId]) {
/******/ 						return {
/******/ 							type: "declined",
/******/ 							chain: chain.concat([parentId]),
/******/ 							moduleId: moduleId,
/******/ 							parentId: parentId
/******/ 						};
/******/ 					}
/******/ 					if (outdatedModules.includes(parentId)) continue;
/******/ 					if (parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if (!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push({
/******/ 						chain: chain.concat([parentId]),
/******/ 						id: parentId
/******/ 					});
/******/ 				}
/******/ 			}
/******/
/******/ 			return {
/******/ 				type: "accepted",
/******/ 				moduleId: updateModuleId,
/******/ 				outdatedModules: outdatedModules,
/******/ 				outdatedDependencies: outdatedDependencies
/******/ 			};
/******/ 		}
/******/
/******/ 		function addAllToSet(a, b) {
/******/ 			for (var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if (!a.includes(item)) a.push(item);
/******/ 			}
/******/ 		}
/******/
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/
/******/ 		var warnUnexpectedRequire = function warnUnexpectedRequire() {
/******/ 			console.warn(
/******/ 				"[HMR] unexpected require(" + result.moduleId + ") to disposed module"
/******/ 			);
/******/ 		};
/******/
/******/ 		for (var id in hotUpdate) {
/******/ 			if (Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				moduleId = toModuleId(id);
/******/ 				var result;
/******/ 				if (hotUpdate[id]) {
/******/ 					result = getAffectedStuff(moduleId);
/******/ 				} else {
/******/ 					result = {
/******/ 						type: "disposed",
/******/ 						moduleId: id
/******/ 					};
/******/ 				}
/******/ 				var abortError = false;
/******/ 				var doApply = false;
/******/ 				var doDispose = false;
/******/ 				var chainInfo = "";
/******/ 				if (result.chain) {
/******/ 					chainInfo = "\nUpdate propagation: " + result.chain.join(" -> ");
/******/ 				}
/******/ 				switch (result.type) {
/******/ 					case "self-declined":
/******/ 						if (options.onDeclined) options.onDeclined(result);
/******/ 						if (!options.ignoreDeclined)
/******/ 							abortError = new Error(
/******/ 								"Aborted because of self decline: " +
/******/ 									result.moduleId +
/******/ 									chainInfo
/******/ 							);
/******/ 						break;
/******/ 					case "declined":
/******/ 						if (options.onDeclined) options.onDeclined(result);
/******/ 						if (!options.ignoreDeclined)
/******/ 							abortError = new Error(
/******/ 								"Aborted because of declined dependency: " +
/******/ 									result.moduleId +
/******/ 									" in " +
/******/ 									result.parentId +
/******/ 									chainInfo
/******/ 							);
/******/ 						break;
/******/ 					case "unaccepted":
/******/ 						if (options.onUnaccepted) options.onUnaccepted(result);
/******/ 						if (!options.ignoreUnaccepted)
/******/ 							abortError = new Error(
/******/ 								"Aborted because " + moduleId + " is not accepted" + chainInfo
/******/ 							);
/******/ 						break;
/******/ 					case "accepted":
/******/ 						if (options.onAccepted) options.onAccepted(result);
/******/ 						doApply = true;
/******/ 						break;
/******/ 					case "disposed":
/******/ 						if (options.onDisposed) options.onDisposed(result);
/******/ 						doDispose = true;
/******/ 						break;
/******/ 					default:
/******/ 						throw new Error("Unexception type " + result.type);
/******/ 				}
/******/ 				if (abortError) {
/******/ 					hotSetStatus("abort");
/******/ 					return Promise.reject(abortError);
/******/ 				}
/******/ 				if (doApply) {
/******/ 					appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 					addAllToSet(outdatedModules, result.outdatedModules);
/******/ 					for (moduleId in result.outdatedDependencies) {
/******/ 						if (
/******/ 							Object.prototype.hasOwnProperty.call(
/******/ 								result.outdatedDependencies,
/******/ 								moduleId
/******/ 							)
/******/ 						) {
/******/ 							if (!outdatedDependencies[moduleId])
/******/ 								outdatedDependencies[moduleId] = [];
/******/ 							addAllToSet(
/******/ 								outdatedDependencies[moduleId],
/******/ 								result.outdatedDependencies[moduleId]
/******/ 							);
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 				if (doDispose) {
/******/ 					addAllToSet(outdatedModules, [result.moduleId]);
/******/ 					appliedUpdate[moduleId] = warnUnexpectedRequire;
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for (i = 0; i < outdatedModules.length; i++) {
/******/ 			moduleId = outdatedModules[i];
/******/ 			if (
/******/ 				installedModules[moduleId] &&
/******/ 				installedModules[moduleId].hot._selfAccepted
/******/ 			)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		Object.keys(hotAvailableFilesMap).forEach(function(chunkId) {
/******/ 			if (hotAvailableFilesMap[chunkId] === false) {
/******/ 				hotDisposeChunk(chunkId);
/******/ 			}
/******/ 		});
/******/
/******/ 		var idx;
/******/ 		var queue = outdatedModules.slice();
/******/ 		while (queue.length > 0) {
/******/ 			moduleId = queue.pop();
/******/ 			module = installedModules[moduleId];
/******/ 			if (!module) continue;
/******/
/******/ 			var data = {};
/******/
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for (j = 0; j < disposeHandlers.length; j++) {
/******/ 				cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/
/******/ 			// when disposing there is no need to call dispose handler
/******/ 			delete outdatedDependencies[moduleId];
/******/
/******/ 			// remove "parents" references from all children
/******/ 			for (j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if (!child) continue;
/******/ 				idx = child.parents.indexOf(moduleId);
/******/ 				if (idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// remove outdated dependency from module children
/******/ 		var dependency;
/******/ 		var moduleOutdatedDependencies;
/******/ 		for (moduleId in outdatedDependencies) {
/******/ 			if (
/******/ 				Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)
/******/ 			) {
/******/ 				module = installedModules[moduleId];
/******/ 				if (module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					for (j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 						dependency = moduleOutdatedDependencies[j];
/******/ 						idx = module.children.indexOf(dependency);
/******/ 						if (idx >= 0) module.children.splice(idx, 1);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/
/******/ 		// insert new code
/******/ 		for (moduleId in appliedUpdate) {
/******/ 			if (Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for (moduleId in outdatedDependencies) {
/******/ 			if (
/******/ 				Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)
/******/ 			) {
/******/ 				module = installedModules[moduleId];
/******/ 				if (module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					var callbacks = [];
/******/ 					for (i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 						dependency = moduleOutdatedDependencies[i];
/******/ 						cb = module.hot._acceptedDependencies[dependency];
/******/ 						if (cb) {
/******/ 							if (callbacks.includes(cb)) continue;
/******/ 							callbacks.push(cb);
/******/ 						}
/******/ 					}
/******/ 					for (i = 0; i < callbacks.length; i++) {
/******/ 						cb = callbacks[i];
/******/ 						try {
/******/ 							cb(moduleOutdatedDependencies);
/******/ 						} catch (err) {
/******/ 							if (options.onErrored) {
/******/ 								options.onErrored({
/******/ 									type: "accept-errored",
/******/ 									moduleId: moduleId,
/******/ 									dependencyId: moduleOutdatedDependencies[i],
/******/ 									error: err
/******/ 								});
/******/ 							}
/******/ 							if (!options.ignoreErrored) {
/******/ 								if (!error) error = err;
/******/ 							}
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// Load self accepted modules
/******/ 		for (i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch (err) {
/******/ 				if (typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch (err2) {
/******/ 						if (options.onErrored) {
/******/ 							options.onErrored({
/******/ 								type: "self-accept-error-handler-errored",
/******/ 								moduleId: moduleId,
/******/ 								error: err2,
/******/ 								originalError: err
/******/ 							});
/******/ 						}
/******/ 						if (!options.ignoreErrored) {
/******/ 							if (!error) error = err2;
/******/ 						}
/******/ 						if (!error) error = err;
/******/ 					}
/******/ 				} else {
/******/ 					if (options.onErrored) {
/******/ 						options.onErrored({
/******/ 							type: "self-accept-errored",
/******/ 							moduleId: moduleId,
/******/ 							error: err
/******/ 						});
/******/ 					}
/******/ 					if (!options.ignoreErrored) {
/******/ 						if (!error) error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if (error) {
/******/ 			hotSetStatus("fail");
/******/ 			return Promise.reject(error);
/******/ 		}
/******/
/******/ 		hotSetStatus("idle");
/******/ 		return new Promise(function(resolve) {
/******/ 			resolve(outdatedModules);
/******/ 		});
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {},
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: (hotCurrentParentsTemp = hotCurrentParents, hotCurrentParents = [], hotCurrentParentsTemp),
/******/ 			children: []
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire("./src/index.ts")(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/images/background.jpg":
/*!**************************************!*\
  !*** ./assets/images/background.jpg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"assets/images/background.jpg\";\n\n//# sourceURL=webpack:///./assets/images/background.jpg?");

/***/ }),

/***/ "./assets/images/logo.png":
/*!********************************!*\
  !*** ./assets/images/logo.png ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"assets/images/logo.png\";\n\n//# sourceURL=webpack:///./assets/images/logo.png?");

/***/ }),

/***/ "./assets/sass/app.scss":
/*!******************************!*\
  !*** ./assets/sass/app.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../../node_modules/css-loader!../../node_modules/sass-loader/lib/loader.js!./app.scss */ \"./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./assets/sass/app.scss\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(true) {\n\tmodule.hot.accept(/*! !../../node_modules/css-loader!../../node_modules/sass-loader/lib/loader.js!./app.scss */ \"./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./assets/sass/app.scss\", function(__WEBPACK_OUTDATED_DEPENDENCIES__) { (function() {\n\t\tvar newContent = __webpack_require__(/*! !../../node_modules/css-loader!../../node_modules/sass-loader/lib/loader.js!./app.scss */ \"./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./assets/sass/app.scss\");\n\n\t\tif(typeof newContent === 'string') newContent = [[module.i, newContent, '']];\n\n\t\tvar locals = (function(a, b) {\n\t\t\tvar key, idx = 0;\n\n\t\t\tfor(key in a) {\n\t\t\t\tif(!b || a[key] !== b[key]) return false;\n\t\t\t\tidx++;\n\t\t\t}\n\n\t\t\tfor(key in b) idx--;\n\n\t\t\treturn idx === 0;\n\t\t}(content.locals, newContent.locals));\n\n\t\tif(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');\n\n\t\tupdate(newContent);\n\t})(__WEBPACK_OUTDATED_DEPENDENCIES__); });\n\n\tmodule.hot.dispose(function() { update(); });\n}\n\n//# sourceURL=webpack:///./assets/sass/app.scss?");

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/json/stringify.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/json/stringify.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nmodule.exports = { \"default\": __webpack_require__(/*! core-js/library/fn/json/stringify */ \"./node_modules/core-js/library/fn/json/stringify.js\"), __esModule: true };\n\n//# sourceURL=webpack:///./node_modules/babel-runtime/core-js/json/stringify.js?");

/***/ }),

/***/ "./node_modules/core-js/library/fn/json/stringify.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/fn/json/stringify.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ \"./node_modules/babel-runtime/core-js/json/stringify.js\");\n\nvar _stringify2 = _interopRequireDefault(_stringify);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar core = __webpack_require__(/*! ../../modules/_core */ \"./node_modules/core-js/library/modules/_core.js\");\nvar $JSON = core.JSON || (core.JSON = { stringify: _stringify2.default });\nmodule.exports = function stringify(it) {\n  // eslint-disable-line no-unused-vars\n  return $JSON.stringify.apply($JSON, arguments);\n};\n\n//# sourceURL=webpack:///./node_modules/core-js/library/fn/json/stringify.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar core = module.exports = { version: '2.5.3' };\nif (typeof __e == 'number') __e = core; // eslint-disable-line no-undef\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_core.js?");

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./assets/sass/app.scss":
/*!*************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./assets/sass/app.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var escape = __webpack_require__(/*! ../../node_modules/css-loader/lib/url/escape.js */ \"./node_modules/css-loader/lib/url/escape.js\");\nexports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ \"./node_modules/css-loader/lib/css-base.js\")(false);\n// imports\n\n\n// module\nexports.push([module.i, \".champions-list-row--clickable {\\n  cursor: pointer; }\\n  .champions-list-row--clickable:hover {\\n    background-color: #ebccd1; }\\n\\n.welcome-logo {\\n  content: url(\" + escape(__webpack_require__(/*! ../images/logo.png */ \"./assets/images/logo.png\")) + \");\\n  max-height: 300px;\\n  padding-bottom: 50px; }\\n\\n.margin-top--10-pixel {\\n  margin-top: 10px; }\\n\\n.margin-bottom--10-pixel {\\n  margin-bottom: 10px; }\\n\\nbody {\\n  background-image: url(\" + escape(__webpack_require__(/*! ../images/background.jpg */ \"./assets/images/background.jpg\")) + \");\\n  background-color: #cccccc;\\n  background-repeat: no-repeat;\\n  background-position: center;\\n  background-blend-mode: exclusion;\\n  background-size: cover; }\\n\", \"\"]);\n\n// exports\n\n\n//# sourceURL=webpack:///./assets/sass/app.scss?./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js");

/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ \"./node_modules/babel-runtime/core-js/json/stringify.js\");\n\nvar _stringify2 = _interopRequireDefault(_stringify);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/*\n\tMIT License http://www.opensource.org/licenses/mit-license.php\n\tAuthor Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\nmodule.exports = function (useSourceMap) {\n\tvar list = [];\n\n\t// return the list of modules as css string\n\tlist.toString = function toString() {\n\t\treturn this.map(function (item) {\n\t\t\tvar content = cssWithMappingToString(item, useSourceMap);\n\t\t\tif (item[2]) {\n\t\t\t\treturn \"@media \" + item[2] + \"{\" + content + \"}\";\n\t\t\t} else {\n\t\t\t\treturn content;\n\t\t\t}\n\t\t}).join(\"\");\n\t};\n\n\t// import a list of modules into the list\n\tlist.i = function (modules, mediaQuery) {\n\t\tif (typeof modules === \"string\") modules = [[null, modules, \"\"]];\n\t\tvar alreadyImportedModules = {};\n\t\tfor (var i = 0; i < this.length; i++) {\n\t\t\tvar id = this[i][0];\n\t\t\tif (typeof id === \"number\") alreadyImportedModules[id] = true;\n\t\t}\n\t\tfor (i = 0; i < modules.length; i++) {\n\t\t\tvar item = modules[i];\n\t\t\t// skip already imported module\n\t\t\t// this implementation is not 100% perfect for weird media query combinations\n\t\t\t//  when a module is imported multiple times with different media queries.\n\t\t\t//  I hope this will never occur (Hey this way we have smaller bundles)\n\t\t\tif (typeof item[0] !== \"number\" || !alreadyImportedModules[item[0]]) {\n\t\t\t\tif (mediaQuery && !item[2]) {\n\t\t\t\t\titem[2] = mediaQuery;\n\t\t\t\t} else if (mediaQuery) {\n\t\t\t\t\titem[2] = \"(\" + item[2] + \") and (\" + mediaQuery + \")\";\n\t\t\t\t}\n\t\t\t\tlist.push(item);\n\t\t\t}\n\t\t}\n\t};\n\treturn list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n\tvar content = item[1] || '';\n\tvar cssMapping = item[3];\n\tif (!cssMapping) {\n\t\treturn content;\n\t}\n\n\tif (useSourceMap && typeof btoa === 'function') {\n\t\tvar sourceMapping = toComment(cssMapping);\n\t\tvar sourceURLs = cssMapping.sources.map(function (source) {\n\t\t\treturn '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';\n\t\t});\n\n\t\treturn [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n\t}\n\n\treturn [content].join('\\n');\n}\n\n// Adapted from convert-source-map (MIT)\nfunction toComment(sourceMap) {\n\t// eslint-disable-next-line no-undef\n\tvar base64 = btoa(unescape(encodeURIComponent((0, _stringify2.default)(sourceMap))));\n\tvar data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;\n\n\treturn '/*# ' + data + ' */';\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/lib/css-base.js?");

/***/ }),

/***/ "./node_modules/css-loader/lib/url/escape.js":
/*!***************************************************!*\
  !*** ./node_modules/css-loader/lib/url/escape.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nmodule.exports = function escape(url) {\n    if (typeof url !== 'string') {\n        return url;\n    }\n    // If url is already wrapped in quotes, remove them\n    if (/^['\"].*['\"]$/.test(url)) {\n        url = url.slice(1, -1);\n    }\n    // Should url be wrapped?\n    // See https://drafts.csswg.org/css-values-3/#urls\n    if (/[\"'() \\t\\n]/.test(url)) {\n        return '\"' + url.replace(/\"/g, '\\\\\"').replace(/\\n/g, '\\\\n') + '\"';\n    }\n\n    return url;\n};\n\n//# sourceURL=webpack:///./node_modules/css-loader/lib/url/escape.js?");

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/*\n\tMIT License http://www.opensource.org/licenses/mit-license.php\n\tAuthor Tobias Koppers @sokra\n*/\n\nvar stylesInDom = {};\n\nvar\tmemoize = function (fn) {\n\tvar memo;\n\n\treturn function () {\n\t\tif (typeof memo === \"undefined\") memo = fn.apply(this, arguments);\n\t\treturn memo;\n\t};\n};\n\nvar isOldIE = memoize(function () {\n\t// Test for IE <= 9 as proposed by Browserhacks\n\t// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n\t// Tests for existence of standard globals is to allow style-loader\n\t// to operate correctly into non-standard environments\n\t// @see https://github.com/webpack-contrib/style-loader/issues/177\n\treturn window && document && document.all && !window.atob;\n});\n\nvar getTarget = function (target) {\n  return document.querySelector(target);\n};\n\nvar getElement = (function (fn) {\n\tvar memo = {};\n\n\treturn function(target) {\n                // If passing function in options, then use it for resolve \"head\" element.\n                // Useful for Shadow Root style i.e\n                // {\n                //   insertInto: function () { return document.querySelector(\"#foo\").shadowRoot }\n                // }\n                if (typeof target === 'function') {\n                        return target();\n                }\n                if (typeof memo[target] === \"undefined\") {\n\t\t\tvar styleTarget = getTarget.call(this, target);\n\t\t\t// Special case to return head of iframe instead of iframe itself\n\t\t\tif (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n\t\t\t\ttry {\n\t\t\t\t\t// This will throw an exception if access to iframe is blocked\n\t\t\t\t\t// due to cross-origin restrictions\n\t\t\t\t\tstyleTarget = styleTarget.contentDocument.head;\n\t\t\t\t} catch(e) {\n\t\t\t\t\tstyleTarget = null;\n\t\t\t\t}\n\t\t\t}\n\t\t\tmemo[target] = styleTarget;\n\t\t}\n\t\treturn memo[target]\n\t};\n})();\n\nvar singleton = null;\nvar\tsingletonCounter = 0;\nvar\tstylesInsertedAtTop = [];\n\nvar\tfixUrls = __webpack_require__(/*! ./urls */ \"./node_modules/style-loader/lib/urls.js\");\n\nmodule.exports = function(list, options) {\n\tif (typeof DEBUG !== \"undefined\" && DEBUG) {\n\t\tif (typeof document !== \"object\") throw new Error(\"The style-loader cannot be used in a non-browser environment\");\n\t}\n\n\toptions = options || {};\n\n\toptions.attrs = typeof options.attrs === \"object\" ? options.attrs : {};\n\n\t// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n\t// tags it will allow on a page\n\tif (!options.singleton && typeof options.singleton !== \"boolean\") options.singleton = isOldIE();\n\n\t// By default, add <style> tags to the <head> element\n        if (!options.insertInto) options.insertInto = \"head\";\n\n\t// By default, add <style> tags to the bottom of the target\n\tif (!options.insertAt) options.insertAt = \"bottom\";\n\n\tvar styles = listToStyles(list, options);\n\n\taddStylesToDom(styles, options);\n\n\treturn function update (newList) {\n\t\tvar mayRemove = [];\n\n\t\tfor (var i = 0; i < styles.length; i++) {\n\t\t\tvar item = styles[i];\n\t\t\tvar domStyle = stylesInDom[item.id];\n\n\t\t\tdomStyle.refs--;\n\t\t\tmayRemove.push(domStyle);\n\t\t}\n\n\t\tif(newList) {\n\t\t\tvar newStyles = listToStyles(newList, options);\n\t\t\taddStylesToDom(newStyles, options);\n\t\t}\n\n\t\tfor (var i = 0; i < mayRemove.length; i++) {\n\t\t\tvar domStyle = mayRemove[i];\n\n\t\t\tif(domStyle.refs === 0) {\n\t\t\t\tfor (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();\n\n\t\t\t\tdelete stylesInDom[domStyle.id];\n\t\t\t}\n\t\t}\n\t};\n};\n\nfunction addStylesToDom (styles, options) {\n\tfor (var i = 0; i < styles.length; i++) {\n\t\tvar item = styles[i];\n\t\tvar domStyle = stylesInDom[item.id];\n\n\t\tif(domStyle) {\n\t\t\tdomStyle.refs++;\n\n\t\t\tfor(var j = 0; j < domStyle.parts.length; j++) {\n\t\t\t\tdomStyle.parts[j](item.parts[j]);\n\t\t\t}\n\n\t\t\tfor(; j < item.parts.length; j++) {\n\t\t\t\tdomStyle.parts.push(addStyle(item.parts[j], options));\n\t\t\t}\n\t\t} else {\n\t\t\tvar parts = [];\n\n\t\t\tfor(var j = 0; j < item.parts.length; j++) {\n\t\t\t\tparts.push(addStyle(item.parts[j], options));\n\t\t\t}\n\n\t\t\tstylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};\n\t\t}\n\t}\n}\n\nfunction listToStyles (list, options) {\n\tvar styles = [];\n\tvar newStyles = {};\n\n\tfor (var i = 0; i < list.length; i++) {\n\t\tvar item = list[i];\n\t\tvar id = options.base ? item[0] + options.base : item[0];\n\t\tvar css = item[1];\n\t\tvar media = item[2];\n\t\tvar sourceMap = item[3];\n\t\tvar part = {css: css, media: media, sourceMap: sourceMap};\n\n\t\tif(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});\n\t\telse newStyles[id].parts.push(part);\n\t}\n\n\treturn styles;\n}\n\nfunction insertStyleElement (options, style) {\n\tvar target = getElement(options.insertInto)\n\n\tif (!target) {\n\t\tthrow new Error(\"Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.\");\n\t}\n\n\tvar lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];\n\n\tif (options.insertAt === \"top\") {\n\t\tif (!lastStyleElementInsertedAtTop) {\n\t\t\ttarget.insertBefore(style, target.firstChild);\n\t\t} else if (lastStyleElementInsertedAtTop.nextSibling) {\n\t\t\ttarget.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);\n\t\t} else {\n\t\t\ttarget.appendChild(style);\n\t\t}\n\t\tstylesInsertedAtTop.push(style);\n\t} else if (options.insertAt === \"bottom\") {\n\t\ttarget.appendChild(style);\n\t} else if (typeof options.insertAt === \"object\" && options.insertAt.before) {\n\t\tvar nextSibling = getElement(options.insertInto + \" \" + options.insertAt.before);\n\t\ttarget.insertBefore(style, nextSibling);\n\t} else {\n\t\tthrow new Error(\"[Style Loader]\\n\\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\\n Must be 'top', 'bottom', or Object.\\n (https://github.com/webpack-contrib/style-loader#insertat)\\n\");\n\t}\n}\n\nfunction removeStyleElement (style) {\n\tif (style.parentNode === null) return false;\n\tstyle.parentNode.removeChild(style);\n\n\tvar idx = stylesInsertedAtTop.indexOf(style);\n\tif(idx >= 0) {\n\t\tstylesInsertedAtTop.splice(idx, 1);\n\t}\n}\n\nfunction createStyleElement (options) {\n\tvar style = document.createElement(\"style\");\n\n\toptions.attrs.type = \"text/css\";\n\n\taddAttrs(style, options.attrs);\n\tinsertStyleElement(options, style);\n\n\treturn style;\n}\n\nfunction createLinkElement (options) {\n\tvar link = document.createElement(\"link\");\n\n\toptions.attrs.type = \"text/css\";\n\toptions.attrs.rel = \"stylesheet\";\n\n\taddAttrs(link, options.attrs);\n\tinsertStyleElement(options, link);\n\n\treturn link;\n}\n\nfunction addAttrs (el, attrs) {\n\tObject.keys(attrs).forEach(function (key) {\n\t\tel.setAttribute(key, attrs[key]);\n\t});\n}\n\nfunction addStyle (obj, options) {\n\tvar style, update, remove, result;\n\n\t// If a transform function was defined, run it on the css\n\tif (options.transform && obj.css) {\n\t    result = options.transform(obj.css);\n\n\t    if (result) {\n\t    \t// If transform returns a value, use that instead of the original css.\n\t    \t// This allows running runtime transformations on the css.\n\t    \tobj.css = result;\n\t    } else {\n\t    \t// If the transform function returns a falsy value, don't add this css.\n\t    \t// This allows conditional loading of css\n\t    \treturn function() {\n\t    \t\t// noop\n\t    \t};\n\t    }\n\t}\n\n\tif (options.singleton) {\n\t\tvar styleIndex = singletonCounter++;\n\n\t\tstyle = singleton || (singleton = createStyleElement(options));\n\n\t\tupdate = applyToSingletonTag.bind(null, style, styleIndex, false);\n\t\tremove = applyToSingletonTag.bind(null, style, styleIndex, true);\n\n\t} else if (\n\t\tobj.sourceMap &&\n\t\ttypeof URL === \"function\" &&\n\t\ttypeof URL.createObjectURL === \"function\" &&\n\t\ttypeof URL.revokeObjectURL === \"function\" &&\n\t\ttypeof Blob === \"function\" &&\n\t\ttypeof btoa === \"function\"\n\t) {\n\t\tstyle = createLinkElement(options);\n\t\tupdate = updateLink.bind(null, style, options);\n\t\tremove = function () {\n\t\t\tremoveStyleElement(style);\n\n\t\t\tif(style.href) URL.revokeObjectURL(style.href);\n\t\t};\n\t} else {\n\t\tstyle = createStyleElement(options);\n\t\tupdate = applyToTag.bind(null, style);\n\t\tremove = function () {\n\t\t\tremoveStyleElement(style);\n\t\t};\n\t}\n\n\tupdate(obj);\n\n\treturn function updateStyle (newObj) {\n\t\tif (newObj) {\n\t\t\tif (\n\t\t\t\tnewObj.css === obj.css &&\n\t\t\t\tnewObj.media === obj.media &&\n\t\t\t\tnewObj.sourceMap === obj.sourceMap\n\t\t\t) {\n\t\t\t\treturn;\n\t\t\t}\n\n\t\t\tupdate(obj = newObj);\n\t\t} else {\n\t\t\tremove();\n\t\t}\n\t};\n}\n\nvar replaceText = (function () {\n\tvar textStore = [];\n\n\treturn function (index, replacement) {\n\t\ttextStore[index] = replacement;\n\n\t\treturn textStore.filter(Boolean).join('\\n');\n\t};\n})();\n\nfunction applyToSingletonTag (style, index, remove, obj) {\n\tvar css = remove ? \"\" : obj.css;\n\n\tif (style.styleSheet) {\n\t\tstyle.styleSheet.cssText = replaceText(index, css);\n\t} else {\n\t\tvar cssNode = document.createTextNode(css);\n\t\tvar childNodes = style.childNodes;\n\n\t\tif (childNodes[index]) style.removeChild(childNodes[index]);\n\n\t\tif (childNodes.length) {\n\t\t\tstyle.insertBefore(cssNode, childNodes[index]);\n\t\t} else {\n\t\t\tstyle.appendChild(cssNode);\n\t\t}\n\t}\n}\n\nfunction applyToTag (style, obj) {\n\tvar css = obj.css;\n\tvar media = obj.media;\n\n\tif(media) {\n\t\tstyle.setAttribute(\"media\", media)\n\t}\n\n\tif(style.styleSheet) {\n\t\tstyle.styleSheet.cssText = css;\n\t} else {\n\t\twhile(style.firstChild) {\n\t\t\tstyle.removeChild(style.firstChild);\n\t\t}\n\n\t\tstyle.appendChild(document.createTextNode(css));\n\t}\n}\n\nfunction updateLink (link, options, obj) {\n\tvar css = obj.css;\n\tvar sourceMap = obj.sourceMap;\n\n\t/*\n\t\tIf convertToAbsoluteUrls isn't defined, but sourcemaps are enabled\n\t\tand there is no publicPath defined then lets turn convertToAbsoluteUrls\n\t\ton by default.  Otherwise default to the convertToAbsoluteUrls option\n\t\tdirectly\n\t*/\n\tvar autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;\n\n\tif (options.convertToAbsoluteUrls || autoFixUrls) {\n\t\tcss = fixUrls(css);\n\t}\n\n\tif (sourceMap) {\n\t\t// http://stackoverflow.com/a/26603875\n\t\tcss += \"\\n/*# sourceMappingURL=data:application/json;base64,\" + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + \" */\";\n\t}\n\n\tvar blob = new Blob([css], { type: \"text/css\" });\n\n\tvar oldSrc = link.href;\n\n\tlink.href = URL.createObjectURL(blob);\n\n\tif(oldSrc) URL.revokeObjectURL(oldSrc);\n}\n\n\n//# sourceURL=webpack:///./node_modules/style-loader/lib/addStyles.js?");

/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ \"./node_modules/babel-runtime/core-js/json/stringify.js\");\n\nvar _stringify2 = _interopRequireDefault(_stringify);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/**\n * When source maps are enabled, `style-loader` uses a link element with a data-uri to\n * embed the css on the page. This breaks all relative urls because now they are relative to a\n * bundle instead of the current page.\n *\n * One solution is to only use full urls, but that may be impossible.\n *\n * Instead, this function \"fixes\" the relative urls to be absolute according to the current page location.\n *\n * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.\n *\n */\n\nmodule.exports = function (css) {\n\t// get current location\n\tvar location = typeof window !== \"undefined\" && window.location;\n\n\tif (!location) {\n\t\tthrow new Error(\"fixUrls requires window.location\");\n\t}\n\n\t// blank or null?\n\tif (!css || typeof css !== \"string\") {\n\t\treturn css;\n\t}\n\n\tvar baseUrl = location.protocol + \"//\" + location.host;\n\tvar currentDir = baseUrl + location.pathname.replace(/\\/[^\\/]*$/, \"/\");\n\n\t// convert each url(...)\n\t/*\n This regular expression is just a way to recursively match brackets within\n a string.\n \t /url\\s*\\(  = Match on the word \"url\" with any whitespace after it and then a parens\n    (  = Start a capturing group\n      (?:  = Start a non-capturing group\n          [^)(]  = Match anything that isn't a parentheses\n          |  = OR\n          \\(  = Match a start parentheses\n              (?:  = Start another non-capturing groups\n                  [^)(]+  = Match anything that isn't a parentheses\n                  |  = OR\n                  \\(  = Match a start parentheses\n                      [^)(]*  = Match anything that isn't a parentheses\n                  \\)  = Match a end parentheses\n              )  = End Group\n              *\\) = Match anything and then a close parens\n          )  = Close non-capturing group\n          *  = Match anything\n       )  = Close capturing group\n  \\)  = Match a close parens\n \t /gi  = Get all matches, not the first.  Be case insensitive.\n  */\n\tvar fixedCss = css.replace(/url\\s*\\(((?:[^)(]|\\((?:[^)(]+|\\([^)(]*\\))*\\))*)\\)/gi, function (fullMatch, origUrl) {\n\t\t// strip quotes (if they exist)\n\t\tvar unquotedOrigUrl = origUrl.trim().replace(/^\"(.*)\"$/, function (o, $1) {\n\t\t\treturn $1;\n\t\t}).replace(/^'(.*)'$/, function (o, $1) {\n\t\t\treturn $1;\n\t\t});\n\n\t\t// already a full url? no change\n\t\tif (/^(#|data:|http:\\/\\/|https:\\/\\/|file:\\/\\/\\/|\\s*$)/i.test(unquotedOrigUrl)) {\n\t\t\treturn fullMatch;\n\t\t}\n\n\t\t// convert the url to a full url\n\t\tvar newUrl;\n\n\t\tif (unquotedOrigUrl.indexOf(\"//\") === 0) {\n\t\t\t//TODO: should we add protocol?\n\t\t\tnewUrl = unquotedOrigUrl;\n\t\t} else if (unquotedOrigUrl.indexOf(\"/\") === 0) {\n\t\t\t// path should be relative to the base url\n\t\t\tnewUrl = baseUrl + unquotedOrigUrl; // already starts with '/'\n\t\t} else {\n\t\t\t// path should be relative to current directory\n\t\t\tnewUrl = currentDir + unquotedOrigUrl.replace(/^\\.\\//, \"\"); // Strip leading './'\n\t\t}\n\n\t\t// send back the fixed url(...)\n\t\treturn \"url(\" + (0, _stringify2.default)(newUrl) + \")\";\n\t});\n\n\t// send back the fixed css\n\treturn fixedCss;\n};\n\n//# sourceURL=webpack:///./node_modules/style-loader/lib/urls.js?");

/***/ }),

/***/ "./src/champions-list/champions-list-controller.ts":
/*!*********************************************************!*\
  !*** ./src/champions-list/champions-list-controller.ts ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar ChampionsListModel_1 = __webpack_require__(/*! ../model/ChampionsListModel */ \"./src/model/ChampionsListModel.ts\");\r\nvar champion_service_1 = __webpack_require__(/*! ../services/champion-service */ \"./src/services/champion-service.ts\");\r\nvar season_service_1 = __webpack_require__(/*! ../services/season-service */ \"./src/services/season-service.ts\");\r\nvar ChampionsListController = /** @class */ (function () {\r\n    function ChampionsListController(seasonService, championService) {\r\n        this.seasonService = seasonService;\r\n        this.championService = championService;\r\n        this.pageTitle = 'Champions List';\r\n        this.periodStart = '2005';\r\n        this.periodEnd = '2015';\r\n        this.model = new ChampionsListModel_1.ChampionsListModel();\r\n        this.init();\r\n        this.model.activePage = 1;\r\n    }\r\n    ChampionsListController.prototype.init = function () {\r\n        this.model.seasonList = this.seasonService.retrieveSeasons();\r\n        this.populateChampions();\r\n    };\r\n    ChampionsListController.prototype.populateChampions = function () {\r\n        this.model.champions = [];\r\n        for (var year = Number(this.periodStart); year <= Number(this.periodEnd); year++) {\r\n            this.model.champions.push(this.championService.retrieveChampion(year));\r\n        }\r\n    };\r\n    ChampionsListController.$inject = [season_service_1.SeasonService.serviceId, champion_service_1.ChampionService.serviceId];\r\n    ChampionsListController.controllerId = 'championsListController';\r\n    return ChampionsListController;\r\n}());\r\nexports.ChampionsListController = ChampionsListController;\r\n\n\n//# sourceURL=webpack:///./src/champions-list/champions-list-controller.ts?");

/***/ }),

/***/ "./src/champions-list/champions-list-module.ts":
/*!*****************************************************!*\
  !*** ./src/champions-list/champions-list-module.ts ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular = __webpack_require__(/*! angular */ \"angular\");\r\nvar champions_list_controller_1 = __webpack_require__(/*! ./champions-list-controller */ \"./src/champions-list/champions-list-controller.ts\");\r\nexports.championsListModule = angular.module('formulaOneRecordsApp.championsListModule', []);\r\nexports.championsListModule.controller(champions_list_controller_1.ChampionsListController.controllerId, champions_list_controller_1.ChampionsListController);\r\n\n\n//# sourceURL=webpack:///./src/champions-list/champions-list-module.ts?");

/***/ }),

/***/ "./src/champions-list/champions-list.html":
/*!************************************************!*\
  !*** ./src/champions-list/champions-list.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var angular=window.angular,ngModule;\ntry {ngModule=angular.module([\"ng\"])}\ncatch(e){ngModule=angular.module(\"ng\",[])}\nvar v1=\"<section>\\n<div class=\\\"panel panel-danger\\\">\\n<blockquote class=\\\"panel-heading\\\">\\n{{::vm.pageTitle}}\\n</blockquote>\\n<div class=\\\"panel-body\\\">\\n<div class=\\\"row form-inline\\\">\\n<strong class=\\\"col-xs-2 control-label\\\">Season Period:</strong>\\n<div class=\\\"col-xs-3 form-group\\\">\\n<label class=\\\"control-label\\\" for=\\\"periodStart\\\">From</label>\\n<select class=\\\"form-control\\\" id=\\\"periodStart\\\" ng-model=\\\"vm.periodStart\\\" ng-options=\\\"season for season in vm.model.seasonList.seasons\\\">\\n</select>\\n</div>\\n<div class=\\\"col-xs-3 form-group\\\">\\n<label class=\\\"control-label\\\" for=\\\"periodEnd\\\">To</label>\\n<select class=\\\"form-control\\\" id=\\\"periodEnd\\\" ng-model=\\\"vm.periodEnd\\\" ng-options=\\\"season for season in vm.model.seasonList.getSeasonsFrom(vm.periodStart)\\\">\\n</select>\\n</div>\\n<div class=\\\"col-xs-3 form-group\\\">\\n<a class=\\\"btn btn-default\\\" ng-click=\\\"vm.populateChampions()\\\">\\n<i class=\\\"glyphicon glyphicon-search\\\"></i>\\nFetch\\n</a>\\n</div>\\n</div>\\n<div class=\\\"row\\\"></div>\\n<div class=\\\"container-fluid\\\">\\n<div class=\\\"row\\\">\\n<div class=\\\"col-xs-1\\\"></div>\\n<strong class=\\\"col-xs-2\\\">Season</strong>\\n<strong class=\\\"col-xs-2\\\">Driver</strong>\\n<strong class=\\\"col-xs-2\\\">Constructor</strong>\\n<strong class=\\\"col-xs-2\\\">Points</strong>\\n<strong class=\\\"col-xs-2\\\">Wins</strong>\\n<div class=\\\"col-xs-1\\\"></div>\\n</div>\\n<div class=\\\"row champions-list-row--clickable\\\" ng-controller=\\\"navController as nc\\\" ng-click=\\\"nc.switchView('/seasonResults/'+champion.season)\\\" ng-repeat=\\\"champion in vm.model.getChampions()\\\">\\n<div class=\\\"col-xs-1\\\"></div>\\n<div class=\\\"col-xs-2\\\">{{champion.season}}</div>\\n<div class=\\\"col-xs-2\\\">{{champion.driverStandings.driver.givenName}} {{champion.driverStandings.driver.familyName}}</div>\\n<div class=\\\"col-xs-2\\\">{{champion.driverStandings.sponsor.name}}</div>\\n<div class=\\\"col-xs-2\\\">{{champion.driverStandings.points }}</div>\\n<div class=\\\"col-xs-2\\\">{{champion.driverStandings.wins}}</div>\\n<div class=\\\"col-xs-1\\\"></div>\\n</div>\\n<div class=\\\"row\\\"></div>\\n<div class=\\\"row\\\" ng-if=\\\"vm.model.getPagination().length > 1\\\">\\n<ul class=\\\"col-md-5 col-md-push-6 pagination pagination-sm center-block\\\">\\n<li ng-class=\\\"{'active': page === vm.model.activePage}\\\" ng-repeat=\\\"page in vm.model.getPagination()\\\">\\n<a href=\\\"\\\" ng-click=\\\"vm.model.activePage = page\\\">{{page}}</a>\\n</li>\\n</ul>\\n</div>\\n</div>\\n</div>\\n</div>\\n</section>\";\nvar id1=\"champions-list.html\";\nvar inj=angular.element(window.document).injector();\nif(inj){inj.get(\"$templateCache\").put(id1,v1);}\nelse{ngModule.run([\"$templateCache\",function(c){c.put(id1,v1)}]);}\nmodule.exports=v1;\n\n//# sourceURL=webpack:///./src/champions-list/champions-list.html?");

/***/ }),

/***/ "./src/configs/app-routes.ts":
/*!***********************************!*\
  !*** ./src/configs/app-routes.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar champions_list_controller_1 = __webpack_require__(/*! ../champions-list/champions-list-controller */ \"./src/champions-list/champions-list-controller.ts\");\r\nvar season_results_controller_1 = __webpack_require__(/*! ../season-results/season-results-controller */ \"./src/season-results/season-results-controller.ts\");\r\nvar welcomeTemplate = __webpack_require__(/*! ../welcome/welcome.html */ \"./src/welcome/welcome.html\");\r\nvar championsListTemplate = __webpack_require__(/*! ../champions-list/champions-list.html */ \"./src/champions-list/champions-list.html\");\r\nvar seasonResultsTemplate = __webpack_require__(/*! ../season-results/season-results.html */ \"./src/season-results/season-results.html\");\r\nappRoutes.$inject = ['$routeProvider'];\r\nfunction appRoutes($routeProvider) {\r\n    $routeProvider\r\n        .when('/welcome', { template: welcomeTemplate })\r\n        .when('/champions', { template: championsListTemplate, controller: champions_list_controller_1.ChampionsListController.controllerId, controllerAs: 'vm' })\r\n        .when('/seasonResults/:season', { template: seasonResultsTemplate, controller: season_results_controller_1.SeasonResultsController.controllerId, controllerAs: 'vm' })\r\n        .otherwise({ redirectTo: '/welcome' });\r\n}\r\nexports.appRoutes = appRoutes;\r\n\n\n//# sourceURL=webpack:///./src/configs/app-routes.ts?");

/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular_1 = __webpack_require__(/*! angular */ \"angular\");\r\n__webpack_require__(/*! ../assets/images/logo.png */ \"./assets/images/logo.png\");\r\n__webpack_require__(/*! ../assets/sass/app.scss */ \"./assets/sass/app.scss\");\r\nvar champions_list_module_1 = __webpack_require__(/*! ./champions-list/champions-list-module */ \"./src/champions-list/champions-list-module.ts\");\r\nvar app_routes_1 = __webpack_require__(/*! ./configs/app-routes */ \"./src/configs/app-routes.ts\");\r\nvar season_results_module_1 = __webpack_require__(/*! ./season-results/season-results-module */ \"./src/season-results/season-results-module.ts\");\r\nvar services_module_1 = __webpack_require__(/*! ./services/services-module */ \"./src/services/services-module.ts\");\r\nvar shared_module_1 = __webpack_require__(/*! ./shared/shared-module */ \"./src/shared/shared-module.ts\");\r\nvar formulaOneRecordsApp = angular_1.module('formulaOneRecordsApp', [\r\n    'ngRoute',\r\n    champions_list_module_1.championsListModule.name,\r\n    season_results_module_1.seasonResultsModule.name,\r\n    shared_module_1.sharedModule.name,\r\n    services_module_1.servicesModule.name\r\n]);\r\nformulaOneRecordsApp.config(app_routes_1.appRoutes);\r\n\n\n//# sourceURL=webpack:///./src/index.ts?");

/***/ }),

/***/ "./src/model/Champion.ts":
/*!*******************************!*\
  !*** ./src/model/Champion.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Champion = /** @class */ (function () {\r\n    function Champion() {\r\n    }\r\n    return Champion;\r\n}());\r\nexports.Champion = Champion;\r\n\n\n//# sourceURL=webpack:///./src/model/Champion.ts?");

/***/ }),

/***/ "./src/model/ChampionsListModel.ts":
/*!*****************************************!*\
  !*** ./src/model/ChampionsListModel.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar DEFAULT_ROWS = 15;\r\nvar ChampionsListModel = /** @class */ (function () {\r\n    function ChampionsListModel() {\r\n        var _this = this;\r\n        this.getPagination = function () {\r\n            var pages = [];\r\n            for (var page = 1; page <= Math.ceil(_this.champions.length / DEFAULT_ROWS); page++) {\r\n                pages.push(page);\r\n            }\r\n            return pages;\r\n        };\r\n    }\r\n    ChampionsListModel.prototype.getChampions = function () {\r\n        var startIndex = ((this.activePage - 1) * DEFAULT_ROWS);\r\n        var pageLimit = (this.activePage * DEFAULT_ROWS);\r\n        return this.champions.filter(function (value, index) { return startIndex <= index && index < pageLimit; });\r\n    };\r\n    return ChampionsListModel;\r\n}());\r\nexports.ChampionsListModel = ChampionsListModel;\r\n\n\n//# sourceURL=webpack:///./src/model/ChampionsListModel.ts?");

/***/ }),

/***/ "./src/model/CircuitDetails.ts":
/*!*************************************!*\
  !*** ./src/model/CircuitDetails.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar CircuitDetails = /** @class */ (function () {\r\n    function CircuitDetails() {\r\n    }\r\n    return CircuitDetails;\r\n}());\r\nexports.CircuitDetails = CircuitDetails;\r\n\n\n//# sourceURL=webpack:///./src/model/CircuitDetails.ts?");

/***/ }),

/***/ "./src/model/DriverDetails.ts":
/*!************************************!*\
  !*** ./src/model/DriverDetails.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar DriverDetails = /** @class */ (function () {\r\n    function DriverDetails() {\r\n    }\r\n    return DriverDetails;\r\n}());\r\nexports.DriverDetails = DriverDetails;\r\n\n\n//# sourceURL=webpack:///./src/model/DriverDetails.ts?");

/***/ }),

/***/ "./src/model/DriverStandings.ts":
/*!**************************************!*\
  !*** ./src/model/DriverStandings.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar DriverStandings = /** @class */ (function () {\r\n    function DriverStandings() {\r\n    }\r\n    return DriverStandings;\r\n}());\r\nexports.DriverStandings = DriverStandings;\r\n\n\n//# sourceURL=webpack:///./src/model/DriverStandings.ts?");

/***/ }),

/***/ "./src/model/Location.ts":
/*!*******************************!*\
  !*** ./src/model/Location.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Location = /** @class */ (function () {\r\n    function Location() {\r\n        var _this = this;\r\n        this.getFullLocation = function () { return _this.locality + \", \" + _this.country; };\r\n    }\r\n    return Location;\r\n}());\r\nexports.Location = Location;\r\n\n\n//# sourceURL=webpack:///./src/model/Location.ts?");

/***/ }),

/***/ "./src/model/RaceResults.ts":
/*!**********************************!*\
  !*** ./src/model/RaceResults.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar RaceResults = /** @class */ (function () {\r\n    function RaceResults() {\r\n    }\r\n    return RaceResults;\r\n}());\r\nexports.RaceResults = RaceResults;\r\n\n\n//# sourceURL=webpack:///./src/model/RaceResults.ts?");

/***/ }),

/***/ "./src/model/SeasonList.ts":
/*!*********************************!*\
  !*** ./src/model/SeasonList.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar SeasonList = /** @class */ (function () {\r\n    function SeasonList() {\r\n        var _this = this;\r\n        this.getSeasonsFrom = function (periodStart) {\r\n            return _this.seasons.filter(function (season) { return season > Number(periodStart); });\r\n        };\r\n    }\r\n    return SeasonList;\r\n}());\r\nexports.SeasonList = SeasonList;\r\n\n\n//# sourceURL=webpack:///./src/model/SeasonList.ts?");

/***/ }),

/***/ "./src/model/SeasonResultsModel.ts":
/*!*****************************************!*\
  !*** ./src/model/SeasonResultsModel.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar SeasonResultsModel = /** @class */ (function () {\r\n    function SeasonResultsModel() {\r\n    }\r\n    return SeasonResultsModel;\r\n}());\r\nexports.SeasonResultsModel = SeasonResultsModel;\r\n\n\n//# sourceURL=webpack:///./src/model/SeasonResultsModel.ts?");

/***/ }),

/***/ "./src/model/Sponsor.ts":
/*!******************************!*\
  !*** ./src/model/Sponsor.ts ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Sponsor = /** @class */ (function () {\r\n    function Sponsor() {\r\n    }\r\n    return Sponsor;\r\n}());\r\nexports.Sponsor = Sponsor;\r\n\n\n//# sourceURL=webpack:///./src/model/Sponsor.ts?");

/***/ }),

/***/ "./src/season-results/season-results-controller.ts":
/*!*********************************************************!*\
  !*** ./src/season-results/season-results-controller.ts ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar SeasonResultsModel_1 = __webpack_require__(/*! ../model/SeasonResultsModel */ \"./src/model/SeasonResultsModel.ts\");\r\nvar champion_service_1 = __webpack_require__(/*! ../services/champion-service */ \"./src/services/champion-service.ts\");\r\nvar race_service_1 = __webpack_require__(/*! ../services/race-service */ \"./src/services/race-service.ts\");\r\nvar SeasonResultsController = /** @class */ (function () {\r\n    function SeasonResultsController($routeParams, raceService, championService) {\r\n        var _this = this;\r\n        this.$routeParams = $routeParams;\r\n        this.raceService = raceService;\r\n        this.championService = championService;\r\n        this.model = new SeasonResultsModel_1.SeasonResultsModel();\r\n        this.isChampion = function (driverId) {\r\n            return _this.model.champion.driverStandings && _this.model.champion.driverStandings.driver.driverId === driverId;\r\n        };\r\n        this.season = $routeParams.season;\r\n        this.pageTitle = \"Season Results: \" + this.season;\r\n        this.init();\r\n    }\r\n    SeasonResultsController.prototype.init = function () {\r\n        this.model.races = this.raceService.retrieveRaceResults(this.season);\r\n        this.model.champion = this.championService.retrieveChampion(Number(this.season));\r\n    };\r\n    SeasonResultsController.$inject = ['$routeParams', race_service_1.RaceService.serviceId, champion_service_1.ChampionService.serviceId];\r\n    SeasonResultsController.controllerId = 'seasonResultsController';\r\n    return SeasonResultsController;\r\n}());\r\nexports.SeasonResultsController = SeasonResultsController;\r\n\n\n//# sourceURL=webpack:///./src/season-results/season-results-controller.ts?");

/***/ }),

/***/ "./src/season-results/season-results-module.ts":
/*!*****************************************************!*\
  !*** ./src/season-results/season-results-module.ts ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular = __webpack_require__(/*! angular */ \"angular\");\r\nvar season_results_controller_1 = __webpack_require__(/*! ./season-results-controller */ \"./src/season-results/season-results-controller.ts\");\r\nexports.seasonResultsModule = angular.module('formulaOneRecordsApp.seasonResultsModule', []);\r\nexports.seasonResultsModule.controller(season_results_controller_1.SeasonResultsController.controllerId, season_results_controller_1.SeasonResultsController);\r\n\n\n//# sourceURL=webpack:///./src/season-results/season-results-module.ts?");

/***/ }),

/***/ "./src/season-results/season-results.html":
/*!************************************************!*\
  !*** ./src/season-results/season-results.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var angular=window.angular,ngModule;\ntry {ngModule=angular.module([\"ng\"])}\ncatch(e){ngModule=angular.module(\"ng\",[])}\nvar v1=\"<section>\\n<div class=\\\"panel panel-danger\\\">\\n<blockquote class=\\\"panel-heading\\\">\\n{{::vm.pageTitle}}\\n</blockquote>\\n<div class=\\\"panel-body\\\">\\n<a class=\\\"btn btn-default margin-bottom--10-pixel\\\" href=\\\"\\\" ng-controller=\\\"navController as nc\\\" ng-click=\\\"nc.switchView('/champions')\\\">\\n<i class=\\\"glyphicon glyphicon-chevron-left\\\"></i>\\nBack\\n</a>\\n<div class=\\\"container-fluid\\\">\\n<div class=\\\"row\\\">\\n<strong class=\\\"col-xs-3\\\">Race</strong>\\n<strong class=\\\"col-xs-3\\\">Circuit</strong>\\n<strong class=\\\"col-xs-3\\\">Winner</strong>\\n<strong class=\\\"col-xs-3\\\">Location</strong>\\n</div>\\n<div class=\\\"row\\\" ng-class=\\\"{'bg-info' : vm.isChampion(race.winner.driverId)}\\\" ng-repeat=\\\"race in vm.model.races\\\">\\n<div class=\\\"col-xs-3\\\">{{race.raceName}}</div>\\n<div class=\\\"col-xs-3\\\">{{race.circuit.circuitName}}</div>\\n<div class=\\\"col-xs-3\\\">{{race.winner.getFullName()}}</div>\\n<div class=\\\"col-xs-3\\\">{{race.circuit.location.getFullLocation()}}</div>\\n</div>\\n</div>\\n<a class=\\\"btn btn-default margin-top--10-pixel\\\" href=\\\"\\\" ng-controller=\\\"navController as nc\\\" ng-click=\\\"nc.switchView('/champions')\\\">\\n<i class=\\\"glyphicon glyphicon-chevron-left\\\"></i>\\nBack\\n</a>\\n</div>\\n</div>\\n</section>\\n\";\nvar id1=\"season-results.html\";\nvar inj=angular.element(window.document).injector();\nif(inj){inj.get(\"$templateCache\").put(id1,v1);}\nelse{ngModule.run([\"$templateCache\",function(c){c.put(id1,v1)}]);}\nmodule.exports=v1;\n\n//# sourceURL=webpack:///./src/season-results/season-results.html?");

/***/ }),

/***/ "./src/services/champion-service.ts":
/*!******************************************!*\
  !*** ./src/services/champion-service.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular_1 = __webpack_require__(/*! angular */ \"angular\");\r\nvar Champion_1 = __webpack_require__(/*! ../model/Champion */ \"./src/model/Champion.ts\");\r\nvar DriverDetails_1 = __webpack_require__(/*! ../model/DriverDetails */ \"./src/model/DriverDetails.ts\");\r\nvar DriverStandings_1 = __webpack_require__(/*! ../model/DriverStandings */ \"./src/model/DriverStandings.ts\");\r\nvar Sponsor_1 = __webpack_require__(/*! ../model/Sponsor */ \"./src/model/Sponsor.ts\");\r\nvar ChampionService = /** @class */ (function () {\r\n    function ChampionService($window, $q, $http, $sce) {\r\n        this.$window = $window;\r\n        this.$q = $q;\r\n        this.$http = $http;\r\n        this.$sce = $sce;\r\n    }\r\n    ChampionService.prototype.retrieveChampion = function (year) {\r\n        var _this = this;\r\n        var champion = new Champion_1.Champion();\r\n        if (this.$window.localStorage.getItem(year)) {\r\n            return (champion = JSON.parse(this.$window.localStorage.getItem(year)));\r\n        }\r\n        champion.season = year;\r\n        /**\r\n         * {@link ISCEService} is needed here to mark and wrap the thirty party api as trusted one, since angular 1.6.x.\r\n         */\r\n        var driverStandingsResp = this.$http\r\n            .jsonp(this.$sce.trustAsResourceUrl(\"https://ergast.com/api/f1/\" + year + \"/driverStandings.json\"), { jsonpCallbackParam: 'callback', cache: true })\r\n            .then(function (resp) { return resp.data; });\r\n        this.$q.all([driverStandingsResp]).then(function (results) {\r\n            var standingsList = results[0].MRData.StandingsTable.StandingsLists[0];\r\n            standingsList &&\r\n                populateChampion(standingsList.DriverStandings[0], champion); // short-circuit expression.\r\n            _this.$window.localStorage.setItem(year, JSON.stringify(champion));\r\n        });\r\n        return champion;\r\n    };\r\n    ChampionService.$inject = ['$window', '$q', '$http', '$sce'];\r\n    ChampionService.serviceId = 'championService';\r\n    return ChampionService;\r\n}());\r\nexports.ChampionService = ChampionService;\r\nfunction populateChampion(driverStandingData, champion) {\r\n    var driverStandings = new DriverStandings_1.DriverStandings();\r\n    var driver = new DriverDetails_1.DriverDetails();\r\n    var sponsor = new Sponsor_1.Sponsor();\r\n    angular_1.extend(sponsor, driverStandingData.Constructors[0]);\r\n    angular_1.extend(driver, driverStandingData.Driver);\r\n    driverStandings.points = driverStandingData.points;\r\n    driverStandings.wins = driverStandingData.wins;\r\n    driverStandings.driver = driver;\r\n    driverStandings.sponsor = sponsor;\r\n    champion.driverStandings = driverStandings;\r\n}\r\n\n\n//# sourceURL=webpack:///./src/services/champion-service.ts?");

/***/ }),

/***/ "./src/services/race-service.ts":
/*!**************************************!*\
  !*** ./src/services/race-service.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular_1 = __webpack_require__(/*! angular */ \"angular\");\r\nvar CircuitDetails_1 = __webpack_require__(/*! ../model/CircuitDetails */ \"./src/model/CircuitDetails.ts\");\r\nvar DriverDetails_1 = __webpack_require__(/*! ../model/DriverDetails */ \"./src/model/DriverDetails.ts\");\r\nvar Location_1 = __webpack_require__(/*! ../model/Location */ \"./src/model/Location.ts\");\r\nvar RaceResults_1 = __webpack_require__(/*! ../model/RaceResults */ \"./src/model/RaceResults.ts\");\r\nvar RaceService = /** @class */ (function () {\r\n    function RaceService($window, $http, $q, $sce) {\r\n        this.$window = $window;\r\n        this.$http = $http;\r\n        this.$q = $q;\r\n        this.$sce = $sce;\r\n    }\r\n    RaceService.prototype.retrieveRaceResults = function (season) {\r\n        var races = [];\r\n        // if (this.$window.localStorage.getItem(season)) {\r\n        //   console.log(this.$window.localStorage.getItem(season));\r\n        //   return (races = JSON.parse(this.$window.localStorage.getItem(season))\r\n        //     .races);\r\n        // }\r\n        /**\r\n         * {@link ISCEService} is needed here to mark and wrap the thirty party api as trusted one, since angular 1.6.x.\r\n         */\r\n        var seasonResultsResp = this.$http\r\n            .jsonp(this.$sce.trustAsResourceUrl(\"https://ergast.com/api/f1/\" + season + \"/results/1.json\"), { jsonpCallbackParam: 'callback', cache: true })\r\n            .then(function (resp) { return resp.data; });\r\n        this.$q.all([seasonResultsResp]).then(function (results) {\r\n            var raceResultsData = results[0].MRData.RaceTable.Races;\r\n            raceResultsData.forEach(function (race) { return races.push(populateRaceResults(race)); });\r\n            //   this.$window.localStorage.setItem(season, `{${JSON.stringify(races)}}`);\r\n        });\r\n        return races;\r\n    };\r\n    RaceService.$inject = ['$window', '$http', '$q', '$sce'];\r\n    RaceService.serviceId = 'raceService';\r\n    return RaceService;\r\n}());\r\nexports.RaceService = RaceService;\r\nfunction populateRaceResults(race) {\r\n    var raceResults = new RaceResults_1.RaceResults();\r\n    raceResults.raceName = race.raceName;\r\n    var circuit = new CircuitDetails_1.CircuitDetails();\r\n    circuit.circuitName = race.Circuit.circuitName;\r\n    var location = new Location_1.Location();\r\n    angular_1.extend(location, race.Circuit.Location);\r\n    circuit.location = location;\r\n    raceResults.circuit = circuit;\r\n    var driver = new DriverDetails_1.DriverDetails();\r\n    angular_1.extend(driver, race.Results[0].Driver);\r\n    raceResults.winner = driver;\r\n    return raceResults;\r\n}\r\n\n\n//# sourceURL=webpack:///./src/services/race-service.ts?");

/***/ }),

/***/ "./src/services/season-service.ts":
/*!****************************************!*\
  !*** ./src/services/season-service.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar SeasonList_1 = __webpack_require__(/*! ../model/SeasonList */ \"./src/model/SeasonList.ts\");\r\nvar MAX_LIMIT = 100; // This can be dynamically set by retrieving the totalSeasonsAvailable first.\r\nvar SEASONS_STORAGE_KEY = 'seasons';\r\nvar SeasonService = /** @class */ (function () {\r\n    function SeasonService($window, $http, $q, $sce) {\r\n        this.$window = $window;\r\n        this.$http = $http;\r\n        this.$q = $q;\r\n        this.$sce = $sce;\r\n        /**\r\n         * {@link ISCEService} is needed here to mark and wrap the thirty party api as trusted one, since angular 1.6.x.\r\n         */\r\n        this.seasonListUrl = this.$sce.trustAsResourceUrl('https://ergast.com/api/f1/seasons.json');\r\n    }\r\n    SeasonService.prototype.retrieveSeasons = function () {\r\n        var _this = this;\r\n        var seasonList = new SeasonList_1.SeasonList();\r\n        if (this.$window.localStorage.getItem(SEASONS_STORAGE_KEY)) {\r\n            seasonList.seasons = JSON.parse(this.$window.localStorage.getItem(SEASONS_STORAGE_KEY));\r\n            return seasonList;\r\n        }\r\n        seasonList.seasons = [];\r\n        var seasonListResp = this.$http\r\n            .jsonp(this.seasonListUrl, {\r\n            jsonpCallbackParam: 'callback',\r\n            params: { limit: MAX_LIMIT },\r\n            cache: true\r\n        })\r\n            .then(function (resp) { return resp.data; });\r\n        this.$q.all([seasonListResp]).then(function (results) {\r\n            var seasons = results[0].MRData.SeasonTable.Seasons;\r\n            seasons.forEach(function (seasonObj) { return seasonList.seasons.push(seasonObj.season); });\r\n            _this.$window.localStorage.setItem(SEASONS_STORAGE_KEY, JSON.stringify(seasonList.seasons));\r\n        });\r\n        return seasonList;\r\n    };\r\n    SeasonService.$inject = ['$window', '$http', '$q', '$sce'];\r\n    SeasonService.serviceId = 'seasonService';\r\n    return SeasonService;\r\n}());\r\nexports.SeasonService = SeasonService;\r\n\n\n//# sourceURL=webpack:///./src/services/season-service.ts?");

/***/ }),

/***/ "./src/services/services-module.ts":
/*!*****************************************!*\
  !*** ./src/services/services-module.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular = __webpack_require__(/*! angular */ \"angular\");\r\nvar champion_service_1 = __webpack_require__(/*! ./champion-service */ \"./src/services/champion-service.ts\");\r\nvar race_service_1 = __webpack_require__(/*! ./race-service */ \"./src/services/race-service.ts\");\r\nvar season_service_1 = __webpack_require__(/*! ./season-service */ \"./src/services/season-service.ts\");\r\nexports.servicesModule = angular.module('formulaOneRecordsApp.servicesModule', []);\r\nexports.servicesModule.service(champion_service_1.ChampionService.serviceId, champion_service_1.ChampionService);\r\nexports.servicesModule.service(race_service_1.RaceService.serviceId, race_service_1.RaceService);\r\nexports.servicesModule.service(season_service_1.SeasonService.serviceId, season_service_1.SeasonService);\r\n\n\n//# sourceURL=webpack:///./src/services/services-module.ts?");

/***/ }),

/***/ "./src/shared/nav-controller.ts":
/*!**************************************!*\
  !*** ./src/shared/nav-controller.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar NavController = /** @class */ (function () {\r\n    function NavController($location) {\r\n        this.$location = $location;\r\n    }\r\n    NavController.prototype.switchView = function (viewName) {\r\n        this.$location.path(viewName);\r\n    };\r\n    NavController.$inject = ['$location'];\r\n    NavController.controllerId = 'navController';\r\n    return NavController;\r\n}());\r\nexports.NavController = NavController;\r\n\n\n//# sourceURL=webpack:///./src/shared/nav-controller.ts?");

/***/ }),

/***/ "./src/shared/shared-module.ts":
/*!*************************************!*\
  !*** ./src/shared/shared-module.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("Object.defineProperty(exports, \"__esModule\", { value: true });\r\nvar angular_1 = __webpack_require__(/*! angular */ \"angular\");\r\nvar nav_controller_1 = __webpack_require__(/*! ./nav-controller */ \"./src/shared/nav-controller.ts\");\r\nexports.sharedModule = angular_1.module('formulaOneRecordsApp.sharedModule', []);\r\n/**\r\n * I added this controller to make the route work during a static HTML access. :'(\r\n * specifying the path in <a href> is not working due to relative path access with file:// protocol.\r\n */\r\nexports.sharedModule.controller(nav_controller_1.NavController.controllerId, nav_controller_1.NavController);\r\n\n\n//# sourceURL=webpack:///./src/shared/shared-module.ts?");

/***/ }),

/***/ "./src/welcome/welcome.html":
/*!**********************************!*\
  !*** ./src/welcome/welcome.html ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var angular=window.angular,ngModule;\ntry {ngModule=angular.module([\"ng\"])}\ncatch(e){ngModule=angular.module(\"ng\",[])}\nvar v1=\"<div class=\\\"panel panel-danger\\\">\\n<blockquote class=\\\"panel-heading\\\">\\nWelcome To F1 World Championship Records\\n</blockquote>\\n<div class=\\\"panel-body\\\">\\n<div class=\\\"row\\\">\\n<img class=\\\"img-responsive center-block welcome-logo\\\"/>\\n</div>\\n</div>\\n</div>\\n\";\nvar id1=\"welcome.html\";\nvar inj=angular.element(window.document).injector();\nif(inj){inj.get(\"$templateCache\").put(id1,v1);}\nelse{ngModule.run([\"$templateCache\",function(c){c.put(id1,v1)}]);}\nmodule.exports=v1;\n\n//# sourceURL=webpack:///./src/welcome/welcome.html?");

/***/ }),

/***/ "angular":
/*!**************************!*\
  !*** external "angular" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = angular;\n\n//# sourceURL=webpack:///external_%22angular%22?");

/***/ })

/******/ });