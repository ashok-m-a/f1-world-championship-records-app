describe('App is live', () => {

    it('should see the welcome page', () =>
        browser.get(browser.baseUrl).then(() =>
            expect(element(by.className('panel-heading')).getText()).toEqual('Welcome To F1 World Championship Records')));

});
